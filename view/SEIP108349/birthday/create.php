<?php
use src\bitm\SEIP108349\birthday\Birthday;
$class = new Birthday();
if (isset($_POST['name']) && $_POST['name'] != "") {
	$name = $_POST['name'];
	$date_of_birth = date("Y-m-d", strtotime($_POST['date_of_birth']));
	if($class->create($name, $date_of_birth)) {
		// header("Location: ?view=birthday&action=index");
		?>
		<script type="text/javascript">open("?view=birthday&action=index", "_self");</script>
		<?php
	}
}
?>
<br>
<form class="form-inline" action="" method="post">
	Name : <input class='form-control' type='text' name="name" placeholder='Your Name'>
	Birthday : <input class='form-control' type='date' name="date_of_birth" placeholder='31-12-1970'>
	<input class="btn btn-success" type='submit' value='Save'>
</form>