<?php
use src\bitm\SEIP108349\birthday\Birthday;
$class = new Birthday();
if (isset($_POST['name']) && $_POST['name'] != "") {
	$column_id = $_POST['column_id'];
	$name = $_POST['name'];
	$date_of_birth = date("Y-m-d", strtotime($_POST['date_of_birth']));
	if($class->update($column_id, $name, $date_of_birth)) {
		// header("Location: ?view=birthday&action=index");
		?>
		<script type="text/javascript">open("?view=birthday&action=index", "_self");</script>
		<?php
	}
}
if (isset($_POST['column_id'])) {
	$list = $class->index($_POST['column_id']);
	$name = $list[$_POST['column_id']]['name'];
	$date_of_birth = date("d-m-Y", strtotime($list[$_POST['column_id']]['date_of_birth']));
	?>
	<form class="form-inline" action="" method="post">
		<input class='form-control' type='hidden' name="column_id" value="<?php echo $_POST['column_id']; ?>" placeholder='Your Name'>
		Name : <input class='form-control' type='text' name="name" value="<?php echo $name; ?>" placeholder='Your Name'>
		Birthday : <input class='form-control' type='date' name="date_of_birth" value="<?php echo $date_of_birth; ?>" placeholder='31-12-1970'>
		<input class="btn btn-success" type='submit' value='Save'>
	</form>
	<?php
}
?>