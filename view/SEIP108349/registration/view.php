<?php
use src\bitm\SEIP108349\registration\registration;
$class = new registration();


if (isset($_POST['column_id'])) {
	$list = $class->index($_POST['column_id']);
	if (is_array($list) && !empty($list)) {
		$serial = 0;
		foreach ($list as $key => $row) {
			$serial++;
			$photo = glob("photo/".$row['id']."*");
			?>
			<div class="row">
				<div class="col-md-4">
					<img src="<?php echo $photo[0]."?".time(); ?>" style="width:100%;">
				</div>
				<div class="col-md-8" style="line-height:2.2">
					<div style="text-align:right; width:120px; float:left; padding-right:5px;">Name : </div><div style="padding-left:5px;"><strong><?php echo $row['name']; ?></strong></div>
					<div style="text-align:right; width:120px; float:left; padding-right:5px;">Father's Name : </div><div style="padding-left:5px;"><strong><?php echo $row['fathers_name']; ?></strong></div>
					<div style="text-align:right; width:120px; float:left; padding-right:5px;">Gender : </div><div style="padding-left:5px;"><strong><?php echo $row['gender']; ?></strong></div>
					<div style="text-align:right; width:120px; float:left; padding-right:5px;">Education : </div><div style="padding-left:5px;"><strong><?php echo implode(", ", unserialize($row['education'])); ?></strong></div>
					<div style="text-align:right; width:120px; float:left; padding-right:5px;">Date Of Birth : </div><div style="padding-left:5px;"><strong><?php echo date("d-m-Y", strtotime($row['date_of_birth'])); ?></strong></div>
					<div style="text-align:right; width:120px; float:left; padding-right:5px;">About : </div><div style="padding-left:5px;"><strong><?php echo $row['about']; ?></strong></div>
					<div style="text-align:right; width:120px; float:left; padding-right:5px;">Email : </div><div style="padding-left:5px;"><strong><?php echo $row['email']; ?></strong></div>
					<div style="text-align:right; width:120px; float:left; padding-right:5px;">City : </div><div style="padding-left:5px;"><strong><?php echo $row['city']; ?></strong></div>
					<div style="text-align:right; width:120px; float:left; padding-right:5px;">Interest : </div><div style="padding-left:5px;"><strong><?php echo implode(", ", unserialize($row['interest'])); ?></strong></div>
				</div>
			</div>

			</tr>
			<?php
		}
		?>
		</table>
		<?php
	}
}