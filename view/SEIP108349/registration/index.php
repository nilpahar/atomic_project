<?php
use src\bitm\SEIP108349\registration\registration;
use src\bitm\SEIP108349\utilities;
$class = new registration();
if (isset($_POST['btnDelete'])) {
	$class->delete($_POST['column_id']);
}
####### MESSAGE VIEW ###################
if ($message = utilities::flushMessage("success")) {
	?>
	<div class="alert alert-success alert-dismissible" style="top: 60px; right: 60px; position: absolute;"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>SUCCESS: </strong><?php echo $message; ?></div>
	<?php
}
if ($message = utilities::flushMessage("error")) {
	?>
	<div class="alert alert-error alert-dismissible" style="top: 60px; right: 60px; position: absolute;"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>ERROR: </strong><?php echo $message; ?></div>
	<?php
}
####### MESSAGE VIEW ###################
$list = $class->index();
if (is_array($list) && !empty($list)) {
	?>
	<table class="table">
	<thead>
		<tr>
			<td>Photo</td>
			<td>Name</td>
			<td>Gender</td>
			<td>Father's Name</td>
			<td>Action</td>
		</tr>
	</thead>
	<?php
	$serial = 0;
	foreach ($list as $key => $row) {
		$serial++;
		?>
		<tr>
			<!-- <td><?php echo $serial; ?></td> -->
			<?php
			$photo = glob("photo/".$row['id']."*");
			?>
			<td><img src="<?php echo $photo[0]."?".time(); ?>" style="width:120px;"></td>
			<td><?php echo $row['name']; ?></td>
			<td><?php echo $row['gender']; ?></td>
			<td><?php echo $row['fathers_name']; ?></td>
			<td>
				<form action="?view=<?php echo $_GET['view']; ?>&action=view" method="post" class="form-inline" style="float:left; margin-right:10px;">
					<input type="hidden" name="column_id" value="<?php echo $row['id']; ?>"><input type="submit" class="btn btn-info" name="btnView" value="View">
				</form>
				<form action="?view=<?php echo $_GET['view']; ?>&action=edit" method="post" class="form-inline" style="float:left; margin-right:10px;">
					<input type="hidden" name="column_id" value="<?php echo $row['id']; ?>"><input type="submit" class="btn btn-warning" name="btnEdit" value="Edit">
				</form>
				<form class="form-inline" action="" method="post" style="float:left; margin-right:10px;">
					<input type="hidden" name="column_id" value="<?php echo $row['id']; ?>"><input type="submit" class="btn btn-danger" name="btnDelete" value="Delete">
				</form>
			</td>
			<?php
			?>
		</tr>
		<?php
	}
	?>
	</table>
	<?php
}