<?php
use src\bitm\SEIP108349\registration\registration;
$class = new registration();
if (isset($_POST['name']) && $_POST['name'] != "") {
	echo "<pre>";
	$data = array_merge($_POST, $_FILES);
	var_dump($data);
	echo "</pre>";
	if($class->create($data)) {
		?><script type="text/javascript">open("?view=registration&action=index", "_self");</script><?php
	}
}
?>
<br>
<form class="form-group" action="" method="post" enctype="multipart/form-data">
	<label>Name</label>
	<input class='form-control' type='text' name="name" placeholder='Your Name'>
	<label>Father's Name</label>
	<input class='form-control' type='text' name="fathers_name" placeholder="Your Father's Name">
	<label>Gender</label><br>
	<input type='radio' name="gender" value="Male" id="male">&nbsp;<label for="male">Male</label>
	<input type='radio' name="gender" value="Female" id="female">&nbsp;<label for="female">Female</label><br>
	<label>Education</label><br>
		<input type='checkbox' name="education[]" value="SSC" id="ssc">&nbsp;<label for="ssc">SSC</label>
		<input type='checkbox' name="education[]" value="HSC" id="hsc">&nbsp;<label for="hsc">HSC</label>
		<input type='checkbox' name="education[]" value="BBA" id="bba">&nbsp;<label for="bba">BBA</label>
		<input type='checkbox' name="education[]" value="MBA" id="mba">&nbsp;<label for="mba">MBA</label>
	<br>
	<label>Date Of Birth</label>
	<input class='form-control' type='text' name="date_of_birth" placeholder='Date Of Birth'>
	<label>About</label>
	<textarea class='form-control' name="about" placeholder='About'></textarea>
	<label>Email</label>
	<input class='form-control' type='email' name="email" placeholder='Email'>
	<label>City</label>
	<select class='form-control' name="city">
		<option value="">Select City</option>
		<option value="Dhaka">Dhaka</option>
		<option value="Chittagon">Chittagon</option>
		<option value="Sylhet">Sylhet</option>
		<option value="Barisal">Barisal</option>
	</select>
	<label>Interest</label>
	<select class='form-control' name="interest[]" multiple>
		<option>Travelling</option>
		<option>Writing</option>
		<option>Sports</option>
		<option>Novel Reading</option>
	</select>
	<label>Your Photo</label>
	<input type="file" name="photo">

	<br>
	<input class="btn btn-success" type='submit' value='Save'>
</form>