<?php
use src\bitm\SEIP108349\registration\registration;
$class = new registration();
if (isset($_POST['name']) && $_POST['name'] != "") {
	echo "<pre>";
	$data = array_merge($_POST, $_FILES);
	if($class->update($data)) {
		?><script type="text/javascript">open("?view=registration&action=index", "_self");</script><?php
	}
}
if (isset($_POST['column_id'])) {
	$list = $class->index($_POST['column_id']);
	foreach ($list as $key => $row) {
		$education = unserialize($row['education']);
		$interest = unserialize($row['interest']);
		?>
		<form class="form-group" action="" method="post" enctype="multipart/form-data">
			<input type="hidden" name="column_id" value="<?php echo $row['id']; ?>">
			<label>Name</label>
			<input class='form-control' type='text' name="name" value="<?php echo $row['name']; ?>" placeholder='Your Name'>
			<label>Father's Name</label>
			<input class='form-control' type='text' name="fathers_name" value="<?php echo $row['fathers_name']; ?>" placeholder="Your Father's Name">
			<label>Gender</label><br>
			<input type='radio' name="gender" <?php echo $row['gender']=="Male" ? "checked" : ""; ?> value="Male" id="male">&nbsp;<label for="male">Male</label>
			<input type='radio' name="gender" <?php echo $row['gender']=="Female" ? "checked" : ""; ?> value="Female" id="female">&nbsp;<label for="female">Female</label><br>
			<label>Education</label><br>
				<input type='checkbox' <?php if(in_array("SSC", $education)) { echo "checked"; }; ?> name="education[]" value="SSC" id="ssc">&nbsp;<label for="ssc">SSC</label>
				<input type='checkbox' <?php if(in_array("HSC", $education)) { echo "checked"; }; ?> name="education[]" value="HSC" id="hsc">&nbsp;<label for="hsc">HSC</label>
				<input type='checkbox' <?php if(in_array("BBA", $education)) { echo "checked"; }; ?> name="education[]" value="BBA" id="bba">&nbsp;<label for="bba">BBA</label>
				<input type='checkbox' <?php if(in_array("MBA", $education)) { echo "checked"; }; ?> name="education[]" value="MBA" id="mba">&nbsp;<label for="mba">MBA</label>
			<br>
			<label>Date Of Birth</label>
			<input class='form-control' type='text' name="date_of_birth" value="<?php echo date("d-m-Y", strtotime($row['date_of_birth'])); ?>" placeholder='Date Of Birth'>
			<label>About</label>
			<textarea class='form-control' name="about" placeholder='About'><?php echo $row['about']; ?></textarea>
			<label>Email</label>
			<input class='form-control' type='email' name="email" value="<?php echo $row['email']; ?>" placeholder='Email'>
			<label>City</label>
			<select class='form-control' name="city">
				<option value="">Select City</option>
				<option value="Dhaka" <?php echo $row['city']=="Dhaka" ? 'selected="selected"' : ""; ?>>Dhaka</option>
				<option value="Chittagon" <?php echo $row['city']=="Chittagon" ? 'selected="selected"' : ""; ?>>Chittagon</option>
				<option value="Sylhet" <?php echo $row['city']=="Sylhet" ? 'selected="selected"' : ""; ?>>Sylhet</option>
				<option value="Barisal" <?php echo $row['city']=="Barisal" ? 'selected="selected"' : ""; ?>>Barisal</option>
			</select>
			<label>Interest</label>
			<select class='form-control' name="interest[]" multiple>
				<option <?php if(in_array("Travelling", $interest)) { echo 'selected="selected"'; }; ?>>Travelling</option>
				<option <?php if(in_array("Writing", $interest)) { echo 'selected="selected"'; }; ?>>Writing</option>
				<option <?php if(in_array("Sports", $interest)) { echo 'selected="selected"'; }; ?>>Sports</option>
				<option <?php if(in_array("Novel Reading", $interest)) { echo 'selected="selected"'; }; ?>>Novel Reading</option>
			</select>
			<label>Your Photo</label>
			<input type="file" name="photo">

			<br>
			<input class="btn btn-success" type='submit' value='Save'>
		</form>
		<?php
	}
}
?>