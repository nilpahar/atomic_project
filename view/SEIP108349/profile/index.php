<?php
use src\bitm\SEIP108349\profile\profile;
use src\bitm\SEIP108349\utilities;
$class = new profile();
if (isset($_POST['btnDelete'])) {
	$class->delete($_POST['column_id']);
}
####### MESSAGE VIEW ###################
if ($message = utilities::flushMessage("success")) {
	?>
	<div class="alert alert-success alert-dismissible" style="top: 60px; right: 60px; position: absolute;"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>SUCCESS: </strong><?php echo $message; ?></div>
	<?php
}
if ($message = utilities::flushMessage("error")) {
	?>
	<div class="alert alert-error alert-dismissible" style="top: 60px; right: 60px; position: absolute;"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>ERROR: </strong><?php echo $message; ?></div>
	<?php
}
####### MESSAGE VIEW ###################
$list = $class->index();
if (is_array($list) && !empty($list)) {
	?>
	<table class="table">
	<thead>
		<tr>
			<th>Serial</th>
			<th>Profile Picture</th>
			<th>Name</th>
			<th>Action</th>
		</tr>
	</thead>
	<?php
	$serial = 0;
	foreach ($list as $key => $row) {
		$serial++;
		$photo = glob("profile_photo/".$row['id'].".*");
		?>
		<tr>
			<td><?php echo $serial; ?></td>
			<td>
				<img src="<?php echo $photo[0]; ?>" alt="No Image" style="width:80px;">
			</td>
			<?php
			foreach ($row as $key => $column) {
				if ($key != "id") {
					?><td><?php echo $column; ?></td><?php
				}
			}
			?>
			<td>
				<form action="?view=<?php echo $_GET['view']; ?>&action=edit" method="post" class="form-inline" style="float:left; margin-right:10px;">
					<input type="hidden" name="column_id" value="<?php echo $row['id']; ?>"><input type="submit" class="btn btn-warning" name="btnEdit" value="Edit">
				</form>
				<form class="form-inline" action="" method="post" style="float:left; margin-right:10px;">
					<input type="hidden" name="column_id" value="<?php echo $row['id']; ?>"><input type="submit" class="btn btn-danger" name="btnDelete" value="Delete">
				</form>
			</td>
			<?php
			?>
		</tr>
		<?php
	}
	?>
	</table>
	<?php
}