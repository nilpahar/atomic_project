<?php
use src\bitm\SEIP108349\profile\profile;
$class = new profile();
if (isset($_FILES['photo']) && $_FILES['photo'] != "") {
	$column_id = $_POST['column_id'];
	$photo = $_FILES['photo'];
	if($class->update($column_id, $photo)) {
		?><script type="text/javascript">open("?view=profile&action=index", "_self");</script><?php
	}
}
if (isset($_POST['column_id']) && $_POST['column_id'] > 0) {
	$list = $class->index($_POST['column_id']);
	$name = $list[$_POST['column_id']]['name'];
	$prev_photo = glob("profile_photo/".$_POST['column_id'].".*");
	?>
	<form class="form-inline" action="" method="post" enctype="multipart/form-data">
		<input class='form-control' type='hidden' name="column_id" value="<?php echo $_POST['column_id']; ?>">
		Name : <input class='form-control' type='text' disabled name="name" value="<?php echo $name; ?>" ><br><br>
		Prev Photo : <img src="<?php echo $prev_photo[0]; ?>" style="width:150px;" alt="No Image"><br><br>
		New Photo : <input type="file" name="photo" class="form-control"><br><br>
		<input class="btn btn-success" type='submit' value='Update'>
	</form>
	<?php
}
?>