<?php
use src\bitm\SEIP108349\email\email;
use src\bitm\SEIP108349\utilities;
$class = new email();
if (isset($_POST['btnDelete'])) {
	$class->delete($_POST['column_id']);
}
if (isset($_POST['btnUnsubscribe'])) {
	$class->unsubscribe($_POST['column_id']);
}
if (isset($_POST['btnSubscribe'])) {
	$class->subscribe($_POST['column_id']);
}
####### MESSAGE VIEW ###################
if ($message = utilities::flushMessage("success")) {
	?>
	<div class="alert alert-success alert-dismissible" style="top: 60px; right: 60px; position: absolute;"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>SUCCESS: </strong><?php echo $message; ?></div>
	<?php
}
if ($message = utilities::flushMessage("error")) {
	?>
	<div class="alert alert-error alert-dismissible" style="top: 60px; right: 60px; position: absolute;"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>ERROR: </strong><?php echo $message; ?></div>
	<?php
}
####### MESSAGE VIEW ###################
$list = $class->index();
if (is_array($list) && !empty($list)) {
	?>
	<table class="table">
	<thead>
		<tr>
			<th>Serial</th>
			<th>Name</th>
			<th>Is Subscribed</th>
			<th>Action</th>
		</tr>
	</thead>
	<?php
	$serial = 0;
	foreach ($list as $key => $row) {
		$serial++;
		?>
		<tr>
			<td><?php echo $serial; ?></td>
			<?php
			foreach ($row as $key => $column) {
				if ($key != "id") {
					?><td><?php echo $column; ?></td><?php
				}
			}
			?>
			<td>
				<?php
				if ($row['is_subscribed'] == "YES") {
					?>
					<form action="?view=<?php echo $_GET['view']; ?>&action=index" method="post" class="form-inline" style="float:left; margin-right:10px;">
						<input type="hidden" name="column_id" value="<?php echo $row['id']; ?>"><input type="submit" class="btn btn-info" name="btnUnsubscribe" value="Unsubscribe">
					</form>
					<?php
				} else {
					?>
					<form action="?view=<?php echo $_GET['view']; ?>&action=index" method="post" class="form-inline" style="float:left; margin-right:10px;">
						<input type="hidden" name="column_id" value="<?php echo $row['id']; ?>"><input type="submit" class="btn btn-success" name="btnSubscribe" value="Subscribe">
					</form>
					<?php
				}
				?>
				<form action="?view=<?php echo $_GET['view']; ?>&action=edit" method="post" class="form-inline" style="float:left; margin-right:10px;">
					<input type="hidden" name="column_id" value="<?php echo $row['id']; ?>"><input type="submit" class="btn btn-warning" name="btnEdit" value="Edit">
				</form>
				<form class="form-inline" action="" method="post" style="float:left; margin-right:10px;">
					<input type="hidden" name="column_id" value="<?php echo $row['id']; ?>"><input type="submit" class="btn btn-danger" name="btnDelete" value="Delete">
				</form>
			</td>
			<?php
			?>
		</tr>
		<?php
	}
	?>
	</table>
	<?php
}