<?php
use src\bitm\SEIP108349\book\Book;
$class = new Book();
if (isset($_POST['name']) && $_POST['name'] != "") {
	$column_id = $_POST['column_id'];
	$name = $_POST['name'];
	$author = $_POST['author'];
	$tags = serialize($_POST['tag']);
	if($class->update($column_id, $name, $author, $tags)) {
		// header("Location: ?view=book&action=index");
		?>
		<script type="text/javascript">open("?view=book&action=index", "_self");</script>
		<?php
	}
}
if (isset($_POST['column_id'])) {
	$list = $class->index($_POST['column_id']);
	$name = $list[$_POST['column_id']]['name'];
	$author = $list[$_POST['column_id']]['author'];
	$tags = unserialize($list[$_POST['column_id']]['tags']);
	?>
	<form class="form-inline" action="" method="post">
		<input class='form-control' type='hidden' name="column_id" value="<?php echo $_POST['column_id']; ?>">
		Name : <input class='form-control' type='text' name="name" value="<?php echo $name; ?>" placeholder='Book Name'>
		Author : <input class='form-control' type='date' name="author" value="<?php echo $author; ?>" placeholder='Book author'>
		<br>
		Tag : 
		<input id="php" <?php if(in_array("php", $tags)) { echo "checked"; }; ?> value="php" class='form-control' type='checkbox' name="tag[]">&nbsp;<label for="php">PHP</label>
		<input id="asp" <?php if(in_array("asp", $tags)) { echo "checked"; }; ?> value="asp" class='form-control' type='checkbox' name="tag[]">&nbsp;<label for="asp">ASP</label>
		<input id="javascript" <?php if(in_array("javascript", $tags)) { echo "checked"; }; ?> value="javascript" class='form-control' type='checkbox' name="tag[]">&nbsp;<label for="javascript">Javascript</label>
		<input id="jquery" <?php if(in_array("jquery", $tags)) { echo "checked"; }; ?> value="jquery" class='form-control' type='checkbox' name="tag[]">&nbsp;<label for="jquery">JQuery</label>
		<br>
		<input class="btn btn-success" type='submit' value='Update'>
	</form>
	<?php
}
?>