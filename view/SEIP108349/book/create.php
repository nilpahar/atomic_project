<?php
use src\bitm\SEIP108349\book\Book;
$class = new Book();
if (isset($_POST['name']) && $_POST['name'] != "") {
	$name = $_POST['name'];
	$author = $_POST['author'];
	$tags = serialize($_POST['tag']);
	if($class->create($name, $author, $tags)) {
		// header("Location: ?view=birthday&action=index");
		?>
		<script type="text/javascript">open("?view=book&action=index", "_self");</script>
		<?php
	}
}
?>
<br>
<form class="form-inline" action="" method="post">
	Name : <input class='form-control' type='text' name="name" placeholder='Book Name'>
	Author : <input class='form-control' type='date' name="author" placeholder='Book author'>
	<br>
	Tag : 
	<input id="php" value="php" class='form-control' type='checkbox' name="tag[]">&nbsp;<label for="php">PHP</label>
	<input id="asp" value="asp" class='form-control' type='checkbox' name="tag[]">&nbsp;<label for="asp">ASP</label>
	<input id="javascript" value="javascript" class='form-control' type='checkbox' name="tag[]">&nbsp;<label for="javascript">Javascript</label>
	<input id="jquery" value="jquery" class='form-control' type='checkbox' name="tag[]">&nbsp;<label for="jquery">JQuery</label>
	<br>
	<input class="btn btn-success" type='submit' value='Save'>
</form>