<?php
use src\bitm\SEIP108349\hobby\hobby;
$class = new hobby();
if (isset($_POST['name']) && $_POST['name'] != "") {
	$name = $_POST['name'];
	$hobbies = serialize($_POST['hobbies']);
	if($class->create($name, $hobbies)) {
		?><script type="text/javascript">open("?view=hobby&action=index", "_self");</script><?php
	}
}
?>
<br>
<form class="form-inline" action="" method="post">
	Name : <input class='form-control' type='text' name="name" placeholder='Your Name'>
	<br>
	Hobbies : 
	<input id="Travelling" value="Travelling" class='form-control' type='checkbox' name="hobbies[]">&nbsp;<label for="Travelling">Travelling</label>
	<input id="Cricket" value="Cricket" class='form-control' type='checkbox' name="hobbies[]">&nbsp;<label for="Cricket">Cricket</label>
	<input id="Football" value="Football" class='form-control' type='checkbox' name="hobbies[]">&nbsp;<label for="Football">Football</label>
	<input id="Sleeping" value="Sleeping" class='form-control' type='checkbox' name="hobbies[]">&nbsp;<label for="Sleeping">Sleeping</label>
	<input id="Gossiping" value="Gossiping" class='form-control' type='checkbox' name="hobbies[]">&nbsp;<label for="Gossiping">Gossiping</label>
	<br>
	<input class="btn btn-success" type='submit' value='Save'>
</form>