<?php
use src\bitm\SEIP108349\hobby\hobby;
$class = new hobby();
if (isset($_POST['name']) && $_POST['name'] != "") {
	$column_id = $_POST['column_id'];
	$name = $_POST['name'];
	$author = $_POST['author'];
	$hobbies = serialize($_POST['tag']);
	if($class->update($column_id, $name, $author, $hobbies)) {
		?><script type="text/javascript">open("?view=hobby&action=index", "_self");</script><?php
	}
}
if (isset($_POST['column_id'])) {
	$list = $class->index($_POST['column_id']);
	$name = $list[$_POST['column_id']]['name'];
	$hobbies = unserialize($list[$_POST['column_id']]['hobbies']);
	?>
	<form class="form-inline" action="" method="post">
		<input class='form-control' type='hidden' name="column_id" value="<?php echo $_POST['column_id']; ?>">
		Name : <input class='form-control' type='text' name="name" value="<?php echo $name; ?>" placeholder='Your Name'>
		<br>
		Tag : 
		<input id="Travelling" <?php if(in_array("Travelling", $hobbies)) { echo "checked"; }; ?> value="Travelling" class='form-control' type='checkbox' name="hobbies[]">&nbsp;<label for="Travelling">Travelling</label>
		<input id="Cricket" <?php if(in_array("Cricket", $hobbies)) { echo "checked"; }; ?> value="Cricket" class='form-control' type='checkbox' name="hobbies[]">&nbsp;<label for="Cricket">Cricket</label>
		<input id="Football" <?php if(in_array("Football", $hobbies)) { echo "checked"; }; ?> value="Football" class='form-control' type='checkbox' name="hobbies[]">&nbsp;<label for="Football">Football</label>
		<input id="Sleeping" <?php if(in_array("Sleeping", $hobbies)) { echo "checked"; }; ?> value="Sleeping" class='form-control' type='checkbox' name="hobbies[]">&nbsp;<label for="Sleeping">Sleeping</label>
		<input id="Gossiping" <?php if(in_array("Gossiping", $hobbies)) { echo "checked"; }; ?> value="Gossiping" class='form-control' type='checkbox' name="hobbies[]">&nbsp;<label for="Gossiping">Gossiping</label>
		<br>
		<input class="btn btn-success" type='submit' value='Update'>
	</form>
	<?php
}
?>