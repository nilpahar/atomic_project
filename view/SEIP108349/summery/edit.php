<?php
use src\bitm\SEIP108349\summery\summery;
$class = new summery();
if (isset($_POST['name']) && $_POST['name'] != "") {
	$column_id = $_POST['column_id'];
	$name = $_POST['name'];
	$summery = $_POST['summery'];
	if($class->update($column_id, $name, $summery)) {
		?><script type="text/javascript">open("?view=summery&action=index", "_self");</script><?php
	}
}
if (isset($_POST['column_id']) && $_POST['column_id'] > 0) {
	$list = $class->index($_POST['column_id']);
	$name = $list[$_POST['column_id']]['name'];
	$summery = $list[$_POST['column_id']]['summery'];
	?>
	<form class="form-inline" action="" method="post">
		<input class='form-control' type='hidden' name="column_id" value="<?php echo $_POST['column_id']; ?>">
		Company : <input class='form-control' type='text' name="name" value="<?php echo $name; ?>" placeholder='Company Name'><br><br>
		Summery : <textarea class='form-control' name="summery" placeholder='Summery'><?php echo $summery; ?></textarea>
		<br><br>
		<input class="btn btn-success" type='submit' value='Update'>
	</form>
	<?php
}
?>