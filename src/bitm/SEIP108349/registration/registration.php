<?php
namespace src\bitm\SEIP108349\registration;
use src\bitm\SEIP108349\utilities;
use PDO;
class registration {
	public $table = "registration";
		
	public function index($column_id = false) {
		$query = "select * from $this->table";
		if ($column_id) {
			$query .= " where id=$column_id";
		}
		$result = utilities::$db->query($query);
		while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
			$content[$row['id']] = $row;
		}
		if (isset($content)) {
			return $content;
		} else {
			return $content = array();
		}
	}
	
	public function create($data = array()) {
		if (is_array($data) && !empty($data)) {
			$query = "insert into $this->table (name, fathers_name, gender, education, date_of_birth, about, email, city, interest) values ('".$data['name']."', '".$data['fathers_name']."', '".$data['gender']."', '".serialize($data['education'])."', '".date("Y-m-d", strtotime($data['date_of_birth']))."', '".$data['about']."', '".$data['email']."', '".$data['city']."', '".serialize($data['interest'])."')";
			if(utilities::$db->query($query)) {
				$lastInsertId = Utilities::$db->lastInsertId();
				if (isset($data['photo']) && $data['photo']['error']==0) {
					$photo_dir = "photo";
					if (!file_exists($photo_dir)) {
						mkdir($photo_dir, 0777, true);
					}
					$photo_path = pathinfo($data['photo']['name']);
					$photo_extension = $photo_path['extension'];
					move_uploaded_file($data['photo']['tmp_name'], $photo_dir."/".$lastInsertId.".".$photo_extension);
				}

				$content = "Successfully stored";
				utilities::setMessage($content, "success");
				return $content;
			} else {
				$content = "Inserting problem";
				utilities::setMessage($content, "error");
				return $content;
			}
		}
	}
	
	public function update($data = array()) {
		if (is_array($data) && !empty($data)) {
			if(array_key_exists("column_id", $data)) {
			$query = "update $this->table set name = '".$data['name']."', fathers_name = '".$data['fathers_name']."', gender = '".$data['gender']."', education = '".serialize($data['education'])."', date_of_birth = '".date("Y-m-d", strtotime($data['date_of_birth']))."', about = '".$data['about']."', email = '".$data['email']."', city = '".$data['city']."', interest = '".serialize($data['interest'])."' where id=".$data['column_id'];
				if(utilities::$db->query($query)) {
					$lastInsertId = $data['column_id'];
					if (isset($data['photo']) && $data['photo']['error']==0) {
						$photo_dir = "photo";
						if (!file_exists($photo_dir)) {
							mkdir($photo_dir, 0777, true);
						}
						$photo_path = pathinfo($data['photo']['name']);
						$photo_extension = $photo_path['extension'];
						move_uploaded_file($data['photo']['tmp_name'], $photo_dir."/".$lastInsertId.".".$photo_extension);
					}

					$content = "Successfully updated";
					utilities::setMessage($content, "success");
					return $content;
				} else {
					$content = "Updating problem";
					utilities::setMessage($content, "error");
					return $content;
				}
			}
		}
	}
	
	public function delete($column_id) {
		$query = "delete from $this->table where id=$column_id";
		if(utilities::$db->query($query)) {
			$content = "Successfully deleted";
			utilities::setMessage($content, "error");
			return $content;
		} else {
			$content = "Deleting problem";	
			utilities::setMessage($content, "error");
			return $content;	
		}
	}
}
?>