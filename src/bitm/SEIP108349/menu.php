<?php
namespace src\bitm\SEIP108349;
/**
* Utilities Class
*/
class menu  { 
	public function header_menu() {
		?>
		<div class="navbar navbar-inverse navbar-fixed-top">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="?">Atomic Project</a>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li><a href="?">Home</a></li>
						<li class="dropdown"><a class="dropdown-toggle" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" href="#">Birthday</a>
						<?php $module_for = "birthday"; ?>
							<ul class="dropdown-menu">
								<li><a href="?view=<?php echo $module_for; ?>&action=index"><?php echo ucwords($module_for); ?> Home</a></li>
								<li><a href="?view=<?php echo $module_for; ?>&action=create">Add New</a></li>
							</ul>
						</li>
						<li class="dropdown"><a class="dropdown-toggle" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" href="#">Book</a>
							<?php $module_for = "book"; ?>
							<ul class="dropdown-menu">
								<li><a href="?view=<?php echo $module_for; ?>&action=index"><?php echo ucwords($module_for); ?> Home</a></li>
								<li><a href="?view=<?php echo $module_for; ?>&action=create">Add New</a></li>
							</ul>
						</li>
						<li class="dropdown"><a class="dropdown-toggle" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" href="#">Email</a>
							<?php $module_for = "email"; ?>
							<ul class="dropdown-menu">
								<li><a href="?view=<?php echo $module_for; ?>&action=index"><?php echo ucwords($module_for); ?> Home</a></li>
								<li><a href="?view=<?php echo $module_for; ?>&action=create">Add New</a></li>
							</ul>
						</li>
						<li class="dropdown"><a class="dropdown-toggle" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" href="#">Hobby</a>
							<?php $module_for = "hobby"; ?>
							<ul class="dropdown-menu">
								<li><a href="?view=<?php echo $module_for; ?>&action=index"><?php echo ucwords($module_for); ?> Home</a></li>
								<li><a href="?view=<?php echo $module_for; ?>&action=create">Add New</a></li>
							</ul>
						</li>
						<li class="dropdown"><a class="dropdown-toggle" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" href="#">Profile</a>
							<?php $module_for = "profile"; ?>
							<ul class="dropdown-menu">
								<li><a href="?view=<?php echo $module_for; ?>&action=index"><?php echo ucwords($module_for); ?> Home</a></li>
								<li><a href="?view=<?php echo $module_for; ?>&action=create">Add New</a></li>
							</ul>
						</li>
						<li class="dropdown"><a class="dropdown-toggle" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" href="#">Summery</a>
							<?php $module_for = "summery"; ?>
							<ul class="dropdown-menu">
								<li><a href="?view=<?php echo $module_for; ?>&action=index"><?php echo ucwords($module_for); ?> Home</a></li>
								<li><a href="?view=<?php echo $module_for; ?>&action=create">Add New</a></li>
							</ul>
						</li>
						<li class="dropdown"><a class="dropdown-toggle" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" href="#">Registration Form</a>
							<?php $module_for = "registration"; ?>
							<ul class="dropdown-menu">
								<li><a href="?view=<?php echo $module_for; ?>&action=index"><?php echo ucwords($module_for); ?> Home</a></li>
								<li><a href="?view=<?php echo $module_for; ?>&action=create">Add New</a></li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<?php
	}
}
?>