<?php
namespace src\bitm\SEIP108349\hobby;
use src\bitm\SEIP108349\utilities;
use PDO;
class hobby {
	public $table = "hobby";
		
	public function index($column_id = false) {
		$query = "select * from $this->table";
		if ($column_id) {
			$query .= " where id=$column_id";
		}
		$result = utilities::$db->query($query);
		while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
			$content[$row['id']] = $row;
		}
		if (isset($content)) {
			return $content;
		} else {
			return $content = array();
		}
	}
	
	public function create($name, $hobbies) {
		$query = "insert into $this->table (name, hobbies) values ('$name', '$hobbies')";
		if(utilities::$db->query($query)) {
			$content = "Successfully stored";
			utilities::setMessage($content, "success");
			return $content;
		} else {
			$content = "Inserting problem";
			utilities::setMessage($content, "error");
			return $content;
		}
	}
	
	public function update($column_id, $name, $hobbies) {
		$query = "update $this->table set name='$name', hobbies='$hobbies' where id=".$column_id;
		if(utilities::$db->query($query)) {
			$content = "Successfully updated";
			utilities::setMessage($content, "success");
			return $content;
		} else {
			$content = "Updating problem";
			utilities::setMessage($content, "error");
			return $content;
		}
	}
	
	public function delete($column_id) {
		$query = "delete from $this->table where id=$column_id";
		if(utilities::$db->query($query)) {
			$content = "Successfully deleted";
			utilities::setMessage($content, "success");
			return $content;
		} else {
			$content = "Deleting problem";
			utilities::setMessage($content, "error");
			return $content;
		}
	}
}
?>