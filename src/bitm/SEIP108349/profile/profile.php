<?php
namespace src\bitm\SEIP108349\profile;
use src\bitm\SEIP108349\utilities;
use PDO;
class profile {
	public $table = "profile";
	public $photo_dir = "profile_photo";
	
	public function index($column_id = false) {
		$query = "select * from $this->table";
		if ($column_id) {
			$query .= " where id=$column_id";
		}
		$result = utilities::$db->query($query);
		while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
			$content[$row['id']] = $row;
		}
		if (isset($content)) {
			return $content;
		} else {
			return $content = array();
		}
	}
	
	public function create($name, $photo) {
		$query = "insert into $this->table (name) values ('$name')";
		if(utilities::$db->query($query)) {
			$lastInsertId = utilities::$db->lastInsertId();
			if (isset($photo) && $photo['error']==0) {
				$photo_path = pathinfo($photo['name']);
				$photo_extension = $photo_path['extension'];
				move_uploaded_file($photo['tmp_name'], $this->photo_dir."/".$lastInsertId.".".$photo_extension);
			}
			$content = "Successfully stored";
			utilities::setMessage($content, "success");
			return $content;
		} else {
			$content = "Inserting problem";
			utilities::setMessage($content, "error");
			return $content;
		}
	}
	
	public function update($column_id, $photo) {
		if (isset($photo) && $photo['error']==0) {
			$photo_path = pathinfo($photo['name']);
			$photo_extension = $photo_path['extension'];
			move_uploaded_file($photo['tmp_name'], $this->photo_dir."/".$column_id.".".$photo_extension);
			$content = "Successfully updated";
			utilities::setMessage($content, "success");
			return $content;
		} else {
			$content = "Updating problem";
			utilities::setMessage($content, "error");
			return $content;
		}
	}
	
	public function delete($column_id) {
		$query = "delete from $this->table where id=$column_id";
		$prev_photo = glob("profile_photo/".$column_id.".*");
		if(utilities::$db->query($query)) {
			if(isset($prev_photo[0])) {
				unlink($prev_photo[0]);
			}
			$content = "Successfully deleted";
			utilities::setMessage($content, "success");
			return $content;
		} else {
			$content = "Deleting problem";		
			utilities::setMessage($content, "error");
			return $content;
		}
	}
}
?>