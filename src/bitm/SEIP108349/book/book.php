<?php
namespace src\bitm\SEIP108349\book;
use src\bitm\SEIP108349\utilities;
use PDO;
class Book {
	public $id;
	public $title ;
	public $created ;
	public $modified ;
	public $created_by;
	public $modified_by;
	public $deleted_at;
	public $table = "book";
		
	public function index($column_id = false) {
		$query = "select * from $this->table";
		if ($column_id) {
			$query .= " where id=$column_id";
		}
		$result = utilities::$db->query($query);
		while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
			$content[$row['id']] = $row;
		}
		if (isset($content)) {
			return $content;
		} else {
			return $content = array();
		}
	}
	
	public function create($name, $author, $tags) {
		$query = "insert into $this->table (name, author, tags) values ('$name', '$author', '$tags')";
		if(utilities::$db->query($query)) {
			$content = "Successfully stored";
			utilities::setMessage($content, "success");
			return $content;
		} else {
			$content = "Inserting problem";
			utilities::setMessage($content, "error");
			return $content;
		}
	}
	
	public function update($column_id, $name, $author, $tags) {
		$query = "update $this->table set name='$name', author='$author', tags='$tags' where id=".$column_id;
		if(utilities::$db->query($query)) {
			$content = "Successfully updated";
			utilities::setMessage($content, "success");
			return $content;
		} else {
			$content = "Updating problem";
			utilities::setMessage($content, "error");
			return $content;
		}
	}
	
	public function delete($column_id) {
		$query = "delete from $this->table where id=$column_id";
		if(utilities::$db->query($query)) {
			$content = "Successfully deleted";
			utilities::setMessage($content, "success");
		} else {
			$content = "Deleting problem";
			utilities::setMessage($content, "error");
		}
		return $content;
	}
}
?>