<?php
namespace src\bitm\SEIP108349\birthday;
use src\bitm\SEIP108349\utilities;
use PDO;
class Birthday {
	public $table = "birthday";
		
	public function index($column_id = false) {
		$query = "select * from $this->table";
		if ($column_id) {
			$query .= " where id=$column_id";
		}
		$result = utilities::$db->query($query);
		while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
			$content[$row['id']] = $row;
		}
		if (isset($content)) {
			return $content;
		} else {
			return $content = array();
		}
	}
	
	public function create($name, $date_of_birth) {
		$query = "insert into $this->table (name, date_of_birth) values ('$name', '$date_of_birth')";
		if(utilities::$db->query($query)) {
			$content = "Successfully stored";
			utilities::setMessage($content, "success");
			return $content;
		} else {
			$content = "Insert problem";
			utilities::setMessage($content, "error");
			return $content;
		}
	}
	
	public function update($column_id, $name, $date_of_birth) {
		$query = "update $this->table set name='$name', date_of_birth='$date_of_birth' where id=".$column_id;
		if(utilities::$db->query($query)) {
			$content = "Successfully updated";
			utilities::setMessage($content, "success");
			return $content;
		} else {
			$content = "Update problem";
			utilities::setMessage($content, "error");
			return $content;
		}
	}
	
	public function delete($column_id) {
		$query = "delete from $this->table where id=$column_id";
		if(utilities::$db->query($query)) {
			$content = "Successfully deleted";
			utilities::setMessage($content, "success");
			return $content;
		} else {
			$content = "Deleting problem";
			utilities::setMessage($content, "error");
			return $content;
		}
	}
}
?>