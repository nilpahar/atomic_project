<?php
namespace src\bitm\SEIP108349\email;
use src\bitm\SEIP108349\utilities;
use PDO;
class email {
	public $table = "email";
	
	public function index($column_id = false) {
		$query = "select * from $this->table";
		if ($column_id) {
			$query .= " where id=$column_id";
		}
		$result = utilities::$db->query($query);
		while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
			$content[$row['id']] = $row;
		}
		if (isset($content)) {
			return $content;
		} else {
			return $content = array();
		}
	}
	
	public function create($email) {
		$query = "insert into $this->table (email) values ('$email')";
		if(utilities::$db->query($query)) {
			$content = "Successfully stored";
			utilities::setMessage($content, "success");
			return $content;
		} else {
			$content = "Inserting problem";
			utilities::setMessage($content, "error");
			return $content;
		}
	}
	
	public function update($column_id, $email) {
		$query = "update $this->table set email='$email' where id=".$column_id;
		if(utilities::$db->query($query)) {
			$content = "Successfully updated";
			utilities::setMessage($content, "success");
			return $content;
		} else {
			$content = "Updating problem";
			utilities::setMessage($content, "error");
			return $content;
		}
	}
	
	public function delete($column_id) {
		$query = "delete from $this->table where id=$column_id";
		if(utilities::$db->query($query)) {
			$content = "Successfully deleted";
			utilities::setMessage($content, "success");
			return $content;
		} else {
			$content = "Deleting problem";	
			utilities::setMessage($content, "error");	
			return $content;
		}
	}

	public function unsubscribe($column_id) {
		$query = "update $this->table set is_subscribed='NO' where id=$column_id";
		if(utilities::$db->query($query)) {
			$content = "Successfully unsubscribed";
			utilities::setMessage($content, "success");
			return $content;
		} else {
			$content = "Unsubscription problem";
			utilities::setMessage($content, "error");		
			return $content;
		}
	}
	
	public function subscribe($column_id) {
		$query = "update $this->table set is_subscribed='YES' where id=$column_id";
		if(utilities::$db->query($query)) {
			$content = "Successfully Subscribed";
			utilities::setMessage($content, "success");
			return $content;
		} else {
			$content = "Subscription problem";	
			utilities::setMessage($content, "error");	
			return $content;
		}
	}
}
?>