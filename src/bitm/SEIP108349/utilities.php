<?php
namespace src\bitm\SEIP108349;
/**
* Utilities Class
*/
use PDO;
class utilities  {
	public static $link = "";
	public static $db = null;
 
	public static function connect($host="localhost", $user="root", $pass="", $dbname="atomic_project") {
		self::$db = new PDO("mysql:host=".$host.";dbname=".$dbname.";charset=utf8", $user, $pass);
	}

	public static function destroy() {
		utilities::$db = null;
	}

	public static function dump($data = false) {
		echo "<pre>";
		var_dump($data);
		echo "</pre>";
	}

	public static function setMessage($message, $type) {
		if (isset($message) && $message != "") {
			if ($type == "error") {
				$_SESSION['message']['error'] = $message;
			} else {
				$_SESSION['message']['success'] = $message;
			}
		}
	}

	public static function flushMessage($type) {
		if (isset($_SESSION['message'][$type]) && $_SESSION['message'][$type] != "") {
			$message = $_SESSION['message'][$type];
			unset($_SESSION['message'][$type]);
			return $message;
		} else {
			return false;
		}
	}
}
?>