<!DOCTYPE html>
<html>
<head>
	<title>This is atomic project</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
</head>
<body style="padding-top: 70px;padding-bottom: 0px;">
	<?php
	session_start();
	use src\bitm\SEIP108349\utilities;
	use src\bitm\SEIP108349\header;

	function __autoload($classname) {
		$file_name = $classname.".php";
		if(file_exists($file_name)) {
			include_once($file_name);
		} else {
			echo "File not found : <b>".$file_name."</b>";
		}
	}
	utilities::connect();

	$header = new header();
	?>
	<div class="container">
		<div class="row">
		<?php
		if ((isset($_GET['view']) && $_GET['view'] != "") && (isset($_GET['action']) && $_GET['action'] != "")) {
			$file_name = "view/SEIP108349/".$_GET['view']."/".$_GET['action'].".php";
			if (file_exists($file_name)) {
				include_once($file_name);
			} else {
				echo "File not found : <b>".$file_name."</b>";
			}
		} else {
			echo "This is home page";
		}
		?>
		</div>
	</div>
	<?php
	utilities::destroy();
	?>
	</body>
</html>
